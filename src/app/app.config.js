/***
 * Configure Main module
 **/

export default config;

config.$inject = ['$urlRouterProvider', '$stateProvider', '$locationProvider','$httpProvider', 'ProfileServiceProvider'];

function config ($urlRouterProvider, $stateProvider, $locationProvider, $httpProvider, ProfileService) {

    ProfileService.loadProfile();
    /**
     * Setup hash bang URL's
     */
    $locationProvider.html5Mode(false);
    $locationProvider.hashPrefix('!');

    /**
     * Configure default routing
     */

    $httpProvider.interceptors.push('LoginInterceptor');

    $urlRouterProvider.when('', '/');
    $urlRouterProvider.otherwise('/');

    $stateProvider.state("app", {
        abstract: true,
        views: {
            app: {template: '<div ui-view></div><footer-main></footer-main>'}
        }
    });
}