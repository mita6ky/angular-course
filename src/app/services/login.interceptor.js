export default LoginInterceptor;

LoginInterceptor.$inject = [];


function LoginInterceptor() {

    return {
        request: function (config) {
            return config;
        },
        response: function (response) {
            var transformResponse = {
                data: {
                    result: response.data,
                    status: response.status
                }
            };
            return transformResponse;
        },
        responseError: function (response) {
            return response;
        }
    };
}
