export default BoardsService;
BoardsService.$inject = ['$resource'];

/* @ngInject */
function BoardsService($resource) {

    var URL = 'http://57e0fb4e4ed1d8110064d494.mockapi.io/api/v1/users/:userId/:action/:boardId/:createCard';
    var defaultParams = {
        userId: '@userId',
        action: '@action',
        boardId: '@boardId',
        createCard: '@createCard'
    };

    ////////////////

    return $resource(URL, defaultParams, {

        getBoards: {
            method: 'GET',
            params: {
                action: 'boards'
            },
            isArray: false
        },

        deleteBoard: {
            method: 'DELETE',
            params: {
                action: 'boards'
            },
            isArray: false
        },

        getBoard: {
            method: 'GET',
            params: {
                action: 'boards'
            },
            isArray: false
        },

        createBoard: {
            method: 'POST',
            params: {
                action: 'boards'
            },
            isArray: false
        },

        createCard: {
            method: 'POST',
            params: {
                action: 'boards',
                createCard: 'cards'
            },
            isArray: false
        }
    });
}