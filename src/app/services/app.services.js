import 'angular';
import UserService from './user.service';
import BoardsService from './board.service';
import LoginInterceptor from './login.interceptor';
import SessionService from './session.service';

export default angular.module('app.api', [])
    .factory('UserService', UserService)
    .factory('BoardsService', BoardsService)
    .factory('LoginInterceptor', LoginInterceptor)
    .service('SessionService', SessionService)
    .name;