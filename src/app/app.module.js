/***
 * Libraries
 */
import 'angular';
import 'ui-router';
import 'ng-resource';
import 'ng-storage';

/***
 * App config
 */
import config from './app.config';

/***
 * App directives
 */
import commonDirectives from './app-directives/common.directives.js';
/***
 * App modules
 */
import clientModule from './client/client.module';
import adminModule from './admin/admin.module';
/***
 * App services
 */
import appServices from './services/app.services';
/***
 * App providers
 */
import appProviders from './providers/app.providers';
/***
 * App filters
 */
import appFilters from './filters/app.filters';
/***
 * App constants
 */
import appConstants from './constants/app.constants';

/**
 * Define application dependencies
 * @type {Array}
 */
let appDependencies = [
    'ui.router',
    'ngResource',
    'ngStorage',
    appServices,
    appProviders,
    appFilters,
    appConstants,
    commonDirectives,
    clientModule,
    adminModule
];

/**
 * Main app module
 * @type {angular.Module}
 */
let app = angular.module('app', appDependencies);

app.config(config)
    .run(appRun);

appRun.$inject = ['$rootScope', '$state', 'ProfileService'];

function appRun ($rootScope, $state, ProfileService) {

    $rootScope.$on('$stateChangeStart', handleStateChange);

    function handleStateChange (evnt, toState, toParams, fromState, fromParams, options) {

        if(ProfileService.isLoggedIn() === toState.params.requireLogin) {

        } else {
            evnt.preventDefault();

            if(ProfileService.isLoggedIn()) {
                $state.go('app.admin.dashboard');
            }else {
                $state.go('app.client.home');
            }
        }
    }
}