// Import vendor dependencies
import 'angular';
import ProfileService from './profile.provider';

/**
 * @name app.providers
 * @param {Object Oject} ProfileService :: Vendor Dependes
 * @desc Set and Get Local Storage Data in config Phaze
 */

export default angular
                    .module('app.providers', [])
                    .provider('ProfileService', ProfileService)
                    .name;