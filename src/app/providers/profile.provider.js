ProfileService.$inject = ['$localStorageProvider'];

export default function ProfileService ($localStorage) {

    var userProfileData = {};


    /***
     * Methods available in RUN
     * @type {{isLoggedIn: isLoggedIn}}
     */
    this.$get = function () {

        return {
            isLoggedIn: isLoggedIn,
            setProfile: setProfile,
            getProfile: getProfile,
            logoutProfile: logoutProfile
        };
    };

    function isLoggedIn () {
        return userProfileData.loggedIn;
    }

    /***
     *
     * @param {Object} userdata
     */
    function setProfile (userData) {

        userProfileData = userData;
        userProfileData.loggedIn = true;

        $localStorage.set('loggedIn', userProfileData.loggedIn);
        $localStorage.set('userId', userProfileData.id);
    }

    /***
     * Methods available only in the config
     */
    this.loadProfile = function () {

        userProfileData.loggedIn = $localStorage.get('loggedIn') || false;
        userProfileData.id = $localStorage.get('userId');
    };

    function logoutProfile() {

        userProfileData.loggedIn = false;
        userProfileData.id = null;
        $localStorage.set('loggedIn', false);
        $localStorage.set('userId', null);
    };


    function getProfile() {
        return userProfileData;
    }
}