import 'angular';
/***
 * Configure Home module
 **/
import template from './profile.tpl.html';

export default config;

config.$inject = ['$stateProvider'];

function config ($stateProvider) {

    $stateProvider.state("app.admin.profile", {
        url: "/profile",
        views: {
            admin: {template: template}
        },
        params: {
            requireLogin: true
        }
    });
}