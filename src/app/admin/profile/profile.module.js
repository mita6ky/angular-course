import 'angular';
import config from './profile.config';

export default angular.module('admin.profile', [])
    .config(config).name;