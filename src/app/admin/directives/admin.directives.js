import 'angular';

import headerDirective from './header/header.directive';
import confirmModal from './modals/confirm.modal.directive';

var dependencies = [];

export default angular
                    .module('admin.directives', dependencies)
                    .directive('headerAdmin', headerDirective)
                    .directive('confirmModal', confirmModal)
                    .name;