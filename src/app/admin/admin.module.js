import 'angular';
/***
 * client config
 */
import config from './admin.config';

import adminDirectives from './directives/admin.directives';


/***
 * admin modules
 */
import boardModule from './dashboard/board/board.module';
import cardModule from './dashboard/board/card/card.module';
import dashboardModule from './dashboard/dashboard.module';
import profile from './profile/profile.module';
import logoutModule from './logout/logout.module';

/**
 * Define application dependencies
 * @type {Array}
 */
let adminDependencies = [
    adminDirectives,
    boardModule,
    cardModule,
    dashboardModule,
    logoutModule,
    profile
];

/**
 * Main client module
 * @type {angular.Module}
 */
export default angular.module('app.admin', adminDependencies)
    .config(config).name;