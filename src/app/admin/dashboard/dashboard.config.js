import 'angular';
/***
 * Configure Home module
 **/
import template from './dashboard.tpl.html';
import dashboardControler from './dashboard.controller';

export default config;

config.$inject = ['$stateProvider'];

function config ($stateProvider) {

    $stateProvider.state("app.admin.dashboard", {
        url: "/dashboard",
        views: {
            admin: {
                template: template,
                controller: dashboardControler,
                controllerAs: 'vm'
            }
        },
        params: {
            requireLogin: true
        }
    });
}