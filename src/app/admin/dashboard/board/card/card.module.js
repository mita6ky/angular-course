import 'angular';
import config from './card.config';

export default angular.module('admin.card', [])
    .config(config).name;