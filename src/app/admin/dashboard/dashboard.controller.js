import _ from 'underscore';

export default DashboardControler;

DashboardControler.$inject = ['BoardsService', 'ProfileService'];

function DashboardControler(BoardsService, ProfileService) {
    var vm   = this;
    vm.title = 'Home Controller';

    vm.boards = [];

    vm.ui = {
        boardsLoading: true,
        modal: {
            show : false,
            title: ''
        }
    };
    let userId;


    init();

    ////////////////

    function init() {
        userId = ProfileService.getProfile().id;
        BoardsService.getBoards({userId: userId}, successGetBoards, failGetBoards);

    }

    vm.deleteBoard = function (board) {
        // if(!board.deleting) {
        //     board.deleting = true;
        //     BoardsService.deleteBoard({userId: userId, boardId: board.id}, successDeleteBoard.bind(board), failDeleteBoard);
        // }
        vm.ui.modal.show = true;
        vm.ui.modal.title = board.name;
        vm.ui.modal.itemToDelete = board;
    };

    vm.confirmDelete = function (item){

        if(!item.deleting) {
            item.deleting = true;

            BoardsService.deleteBoard({
                userId: userId,
                boardId: item.id
            }, successDeleteBoard.bind(item), failDeleteBoard.bind(item));
        }
    };

    function successDeleteBoard(response) {

        vm.boards = _.reject(vm.boards, this);
        this.deleting = false;
    }

    function failDeleteBoard(response) {
        //TODO: Show error
        this.deleting = false;
    }

    vm.createBoard = function () {

        var title = prompt("Enter board name: ");

        var newBoard = {
            name: title,
            userId: userId
        };

        BoardsService.createBoard(newBoard,
            function success(response) {
                console.log(response.result);
                BoardsService.getBoards({userId: userId}, successGetBoards, failGetBoards);
            }, function error(response) {
                //TODO: error
            }
        )
    };

    function successGetBoards(response) {
        vm.boards = response.result;
        vm.ui.boardsLoading = false;
    }

    function failGetBoards(response) {
        vm.ui.boardsLoading = false;
    }

    function successDeleteBoard(response) {

        vm.boards = _.reject(vm.boards, this);
        this.deleting = false;
    }

    function failDeleteBoard(response) {
        //TODO: Show error
        board.deleting = false;
    }
}