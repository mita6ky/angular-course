import 'angular';
import config from './dashboard.config';

export default angular.module('admin.dashboard', [])
    .config(config).name;