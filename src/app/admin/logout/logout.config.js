import 'angular';
/***
 * Configure Home module
 **/
import template from './logout.tpl.html';
import logoutController from './logout.controller';

export default config;

config.$inject = ['$stateProvider'];

function config ($stateProvider) {

    $stateProvider.state("app.admin.logout", {
        url: "/logout",
        views: {
            admin: {
                template: template,
                controller: logoutController,
                controllerAs: 'vm'
            }
        },
        params: {
            requireLogin: true
        }
    });
}