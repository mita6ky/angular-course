import 'angular';

import headerClient from './header/header.directive';

var dependencies = [];

export default angular
                    .module('client.directives', dependencies)
                    .directive('headerClient', headerClient)
                    .name;