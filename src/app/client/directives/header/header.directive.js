import template from './header.directive.tpl.html';

headerClient.$inject = [];

export default function headerClient() {
    var directive = {
        restrict: 'E',
        template: template,
        link: link,
        controller: headerController,
        controllerAs: 'vm',
        scope: true
    };


    headerController.$inject = ['$scope'];

    function headerController ($scope) {
        var vm = this;

        vm.title = 'header controller directive';
        console.log('u did it');
    }

    return directive;

    function link() {}
}