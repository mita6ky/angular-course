import 'angular';
/***
 * client config
 */
import config from './client.config';

/***
 * client directives
 */
import clientDirectives from './directives/client.directives';

/***
 * client modules
 */
import homeModule from './home/home.module';
import loginModule from './login/login.module';
import registerModule from './register/register.module';

/**
 * Define application dependencies
 * @type {Array}
 */
let clientDependencies = [
    clientDirectives,
    homeModule,
    loginModule,
    registerModule
];

/**
 * Main client module
 * @type {angular.Module}
 */
export default angular.module('app.client', clientDependencies)
    .config(config).name;