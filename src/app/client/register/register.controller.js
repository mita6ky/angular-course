export default RegisterController;

RegisterController.$inject = ['$scope', 'UserService', 'filterByFilter', '$state'];

function RegisterController ($scope, UserService, filterBy, $state) {

    var vm = this;

    var users = [];

    vm.title = 'Register Controller';

    vm.userData = {
        username: '',
        password: '',
        email: ''
    };

    vm.ui = {
        errors: {
            takenUsername: false,
            takenEmail: false
        }
    };

    activate();

    ////////////////

    function activate() {
        UserService.authenticateUser({}, function success (response) {
            users = response.result;
        });
    }

    vm.initiateRegister = function () {

        console.log(vm.registerForm)
        if( vm.registerForm.$valid) {
            console.log('YES');

            UserService.registerUser(vm.userData,
                function success (response){

                    console.log('USPQ')
                    $state.go('app.admin.dashboard')
                }, function error (response) {

                    console.error(response);
                });

        }else {

            vm.ui.errors.taken = true;
        }
    };

    vm.checkUsername = function () {

        if(vm.registerForm.username.$valid) {

            if(filterBy(users, vm.userData.username)){
                vm.ui.errors.taken = true;
                console.log('HAHA TRY AGAIN!');
            }else {
                vm.ui.errors.taken = false;
                console.log('WELCOME TO THE MATRIX');
            }
        }
    };

    vm.checkEmail = function() {
        if(vm.registerForm.email.$valid) {

           console.log('checking')
            console.log(vm.userData);
        }
    }
}