export default LoginController;

LoginController.$inject = ['$scope', 'UserService', 'checkUserFilter', '$state', 'ProfileService'];

function LoginController ($scope, UserService, checkUser, $state, ProfileService) {

    var vm = this;

    vm.title = 'Login Controller';


    var users = [];

    vm.userData = {
        username: '',
        password: ''
    };

    vm.ui = {
        errors: {
            invalidCredentials: false
        }
    };

    activate();

    ////////////////

    function activate() {
        UserService.authenticateUser({}, function success (response) {
            users = response.result;
        });
    }

    vm.initiateLogin = function () {

        if( vm.loginForm.$valid) {
            var userData = checkUser(users, vm.userData);

            if(userData) {
                ProfileService.setProfile(userData);
                $state.go('app.admin.dashboard');
                vm.ui.errors.invalidCredentials = false;
            } else {
                vm.ui.errors.invalidCredentials = true;
            }
        } else {
            console.log('NO');
            vm.ui.errors.invalidCredentials = true;
        }
    }
}