import template from './footer.directive.tpl.html';

footerMain.$inject = [];

export default function footerMain() {
    var directive = {
        restrict: 'E',
        template: template,
        link: link
    };

    return directive;

    function link() {}
}