import 'angular';

import footerDirective from './footer/footer.directive';

var commonDepends = [];

export default angular
                    .module('common.directives', commonDepends)
                    .directive('footerMain', footerDirective)
                    .name;