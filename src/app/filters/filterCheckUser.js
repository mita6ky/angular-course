import 'angular';
import _ from 'underscore';

export default  angular.module('app.filters.checkUser', [])
    .filter('checkUser', ['$parse', function( $parse ) {
        return function(collection, search) {

            var result = _.findWhere(collection, {
                username: search.username,
                password: search.password
            });

            return result;
        }
    }]).name;