import 'angular';
import FilterBy from './filterBy';
import checkUser from './filterCheckUser';

let deps = [
    FilterBy,
    checkUser
];

export default angular.module('app.filters', deps)
    .name;